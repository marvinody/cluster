package cluster

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"sort"
)

// Result is the struct you get back after clustering
type Result struct {
	Clusters   []*cluster
	Iterations int
	dimensions int
	Data       [][]float64
	args       *args
}

func (result *Result) String() string {
	return fmt.Sprintf(
		"Clusters:%v\ndim:%d\nData:%v\n", result.Clusters, result.dimensions, result.Data)
}

type cluster struct {
	Len      int
	Loc      []float64
	lastLoc  []float64
	Data     [][]float64
	startLoc []float64
	idxs     []int // this is a cheap way to hold the tuples of floats,
	// I'll save the idx in the array and then before the final call, just fill the data array
}

func (c *cluster) String() string {
	return fmt.Sprintf("C[L:%v, D:%v]", c.Loc, c.Data)
}

func (c *cluster) recenter() {
	c.lastLoc = c.Loc
	if len(c.Data) == 0 {
		return
	}
	c.Loc = tupleAverage(c.Data)
}

// Not sure how else to do this in go but to be able to pass arbitrary args to a super function
// I'll have all the subfunctions of course public but to have one that you can change parameters
// and it'll change the algo I think would be really cool to have
// I'll make it work somehow
type args struct {
	clusterAlgo         string
	clusterLocationInit string
	clusterSortResults  bool
	rng                 *rand.Rand
}

// Option provides an outline for how to set args
type Option func(*args) error

// OptionSeedRNG allows you to provide an RNG seed for reproducibility
// Currently, you SHOULD supply it because it doesn't pick one at random
func OptionSeedRNG(n int64) func(*args) error {
	return func(args *args) error {
		args.rng = rand.New(rand.NewSource(n))
		return nil
	}
}

// OptionSortResults will sort the data belonging to each cluster
// Data will be in the order of closest data (to that cluster) first
func OptionSortResults(args *args) error {
	args.clusterSortResults = true
	return nil
}

// OptionAlgoKMeansFlag will force K-means clustering
func OptionAlgoKMeansFlag(args *args) error {
	args.clusterAlgo = "kmeans"
	return nil
}

// OptionKMeansLocRandomFlag will pick random data points to be the centers
// Do not recommend, very close (or the same) data points may be picked and throw off results
// Use the KPP flag for better clustering with K-means
func OptionKMeansLocRandomFlag(args *args) error {
	args.clusterLocationInit = "random"
	return nil
}

// OptionKMeansLocKPPFlag (K-means Loc(ation) K++) will use the K++ algo to seed the initial clusters' location
func OptionKMeansLocKPPFlag(args *args) error {
	args.clusterLocationInit = "kpp"
	return nil
}

func parseOptions(options ...Option) (*args, error) {
	args := &args{}
	args.rng = rand.New(rand.NewSource(rand.Int63()))
	args.clusterLocationInit = "random"
	args.clusterAlgo = "kmeans"
	args.clusterSortResults = false
	for _, opt := range options {
		err := opt(args)
		if err != nil {
			return nil, err
		}
	}

	return args, nil
}

const epsilon float64 = 0.000001 // tolerance for floating point equality

func equalsWithinTol(a, b, tol float64) bool {
	return math.Abs(a-b) < tol
}

func equals(a, b float64) bool {
	return equalsWithinTol(a, b, epsilon)
}

func tupleEqualsWithinTol(A, B []float64, tol float64) bool {
	if A == nil || B == nil {
		return false
	}
	if len(A) != len(B) {
		return false
	}
	for idx := range A {
		if !equalsWithinTol(A[idx], B[idx], tol) {
			return false
		}
	}
	return true

}

func tupleEquals(A, B []float64) bool {
	return tupleEqualsWithinTol(A, B, epsilon)
}

func distanceSq(A, B []float64) float64 {
	total := 0.0
	for d := range A {
		diff := A[d] - B[d]
		//total += math.Pow(diff, 2)
		total += (diff * diff)
	}
	return total
}

func tupleAverage(l [][]float64) []float64 {
	n := len(l)
	total := make([]float64, len(l[0]))
	for _, tuple := range l {
		for d := range tuple {
			total[d] += tuple[d]
		}
	}
	for d := range total {
		total[d] = total[d] / float64(n)
	}
	return total
}

func initializeKMeansClusterLocationsKpp(result *Result) {
	pointIdx := result.args.rng.Intn(len(result.Data))
	copy(result.Clusters[0].Loc, result.Data[pointIdx])
	k := len(result.Clusters)

	// can't use the other one because that takes array of clusters and we don't have all the locations set yet
	distanceToClosestCluster := func(tuple []float64, clustersFound int) float64 {
		shortestDist := math.MaxFloat64
		for j := 0; j < clustersFound; j++ {
			dist := distanceSq(result.Clusters[j].Loc, tuple)
			if dist < shortestDist {
				shortestDist = dist
			}
		}
		return shortestDist
	}
	weightedRandom := func(weight float64) float64 {
		return -(math.Log(1 - result.args.rng.Float64())) / weight
	}

	for i := 1; i < k; i++ { // we start at one because 0 was assigned above
		lowestRandWeight := math.MaxFloat64 // give it max because we want the stuff to be smaller
		lowestIdx := 0
		for idx, tuple := range result.Data {
			randWeight := weightedRandom(distanceToClosestCluster(tuple, i))
			if randWeight < lowestRandWeight {
				lowestIdx = idx
				lowestRandWeight = randWeight
			}
		}
		copy(result.Clusters[i].Loc, result.Data[lowestIdx])
	}

}

// pick a random existing data point
func initializeKMeansClusterLocationsRandom(result *Result) {
	for idx := range result.Clusters {
		pointIdx := result.args.rng.Intn(len(result.Data))
		copy(result.Clusters[idx].Loc, result.Data[pointIdx])
	}
}

func initializeKMeansClusterLocations(result *Result) {
	switch result.args.clusterLocationInit {
	case "random":
		initializeKMeansClusterLocationsRandom(result)
	case "kpp":
		initializeKMeansClusterLocationsKpp(result)

	}
}

func sortResults(result *Result) {
	if !result.args.clusterSortResults {
		return
	}
	for _, cluster := range result.Clusters {
		sort.Slice(cluster.Data, func(i, j int) bool {
			iDist := distanceSq(cluster.Data[i], cluster.Loc)
			jDist := distanceSq(cluster.Data[j], cluster.Loc)
			return iDist < jDist
		})
	}
}

func initializeKMeansClusters(result *Result) {
	for idx := range result.Clusters {
		result.Clusters[idx] = &cluster{}
		result.Clusters[idx].Len = idx
		result.Clusters[idx].Loc = make([]float64, result.dimensions)
		result.Clusters[idx].startLoc = make([]float64, result.dimensions)
		result.Clusters[idx].Data = make([][]float64, 0)
		result.Clusters[idx].idxs = make([]int, 0)
	}

	initializeKMeansClusterLocations(result)
}

func getClosestCluster(tuple []float64, clusters []*cluster) *cluster {
	closestIdx := -1
	distance := math.MaxFloat64

	for idx, cluster := range clusters {
		if curDist := distanceSq(tuple, cluster.Loc); curDist < distance {
			closestIdx = idx
			distance = curDist
		}
	}

	return clusters[closestIdx]
}

//FindKClusters will attempt to group the passed data into K clusters with any passed options
func FindKClusters(k int, data [][]float64, options ...Option) (*Result, error) {
	result := &Result{}
	args, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	result.args = args
	result.Clusters = make([]*cluster, k)
	result.Data = data
	canStop := func() bool { // can stop if lastloc and loc are the same, keep going if different
		for _, cluster := range result.Clusters {

			if !tupleEquals(cluster.lastLoc, cluster.Loc) {
				return false
			}
		}
		return true
	}
	result.dimensions = len(data[0])
	initializeKMeansClusters(result)
	for !canStop() { // while not converged
		for idx := range result.Clusters { // blank out all points belonging to cluster
			result.Clusters[idx].Data = make([][]float64, 0)
		}
		for _, tuple := range result.Data { // go over all data points
			// find closest cluster for given point
			clusterPtr := getClosestCluster(tuple, result.Clusters)
			// update cluster
			clusterPtr.Data = append(clusterPtr.Data, tuple)

		}
		// all points should be in closest cluster now
		// let's set last loc to get loop termination
		// let's find the average of all the points in each cluster and set new loc
		for _, cluster := range result.Clusters {
			cluster.recenter()
		}
		result.Iterations += 1
	}
	// call sort and deal with it if we need to
	sortResults(result)
	return result, nil
}

// FindClusters will try a kmeans clustering with several K's and then pick the best one
func FindClusters(data [][]float64) (*Result, error) {
	return &Result{}, errors.New("Unimplemented")
}
