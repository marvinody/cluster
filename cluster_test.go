package cluster

import (
	"math"
	"math/rand"
	"testing"
)

func TestClusterString(t *testing.T) {
	cluster := &cluster{}
	cluster.Loc = []float64{10, 10}
	cluster.Data = [][]float64{
		[]float64{0},
		[]float64{1},
		[]float64{2},
	}
	expected := "C[L:[10 10], D:[[0] [1] [2]]]"
	if expected != cluster.String() {
		t.Errorf("Did not get expected output for string method\nExpected:%s\nActual  :%s", expected, cluster.String())
	}

}

func benchmarkCluster(dimensions, points int) (*Result, error) {
	boxRange := 10.0
	min := 0.0
	max := 90.0
	// a 2d world will have 4 (2^2) corners
	// a 3d world will have 8 (2^3) corners...
	startCoords := make([][]float64, int(math.Pow(2, float64(dimensions))))
	//d := 0; d < dimensions; d += 1
	for idx := range startCoords {
		startCoords[idx] = make([]float64, 0, dimensions)
		for d := 0; d < dimensions; d += 1 {
			coord := min
			if (idx & (1 << uint(d))) > d {
				coord = max
			}
			startCoords[idx] = append(startCoords[idx], coord)
		}

	}

	data := make([][]float64, 0, points*len(startCoords))
	for _, startTuple := range startCoords {
		for i := 0; i < points; i += 1 {
			sx, sy := rand.Float64(), rand.Float64()
			x := startTuple[0] + (sx * boxRange)
			y := startTuple[1] + (sy * boxRange)
			data = append(data, []float64{x, y})
		}
	}
	k := len(startCoords)
	return FindKClusters(k, data, OptionKMeansLocKPPFlag, OptionSeedRNG(123456789))
}

func BenchmarkCluster2D100N(b *testing.B) {
	result, err := benchmarkCluster(2, 100)
	if err != nil {
		panic(err)
	}
	result.Iterations++

}
func BenchmarkCluster2D1000N(b *testing.B) {
	result, err := benchmarkCluster(2, 1000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}
func BenchmarkCluster2D10000N(b *testing.B) {
	result, err := benchmarkCluster(2, 10000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}
func BenchmarkCluster2D100000N(b *testing.B) {
	result, err := benchmarkCluster(2, 100000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}

func BenchmarkCluster3D100N(b *testing.B) {
	result, err := benchmarkCluster(3, 100)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}
func BenchmarkCluster3D1000N(b *testing.B) {
	result, err := benchmarkCluster(3, 1000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}
func BenchmarkCluster3D10000N(b *testing.B) {
	result, err := benchmarkCluster(3, 10000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}
func BenchmarkCluster3D100000N(b *testing.B) {
	result, err := benchmarkCluster(3, 100000)
	if err != nil {
		panic(err)
	}
	result.Iterations++
}

// just a sanity check since this will be crazy if wrong eventually
func TestEquals(t *testing.T) {
	var a float64 = 2.109613694334124 //"random" number
	var b float64 = a - (epsilon * 0.9)
	// default tol with epsilon default
	if !equals(a, b) {
		t.Error("Expected for 2 floats to be equal within epsilon, but didn't get")
	}
	a, b = 90.0, 94.99999999
	tol := 5.0
	// test regular tol
	if !equalsWithinTol(a, b, tol) {
		t.Errorf("Expected for 2 floats to be equal within tol")
	}
	// test tuple tol
	A, B := []float64{4.999999, 94.999999}, []float64{0, 90}
	if !tupleEqualsWithinTol(A, B, tol) {
		t.Errorf("Expected tuple to be equal within tol")
	}
	// test equals with diff dimensions
	A, B = []float64{0}, []float64{0, 0}
	if tupleEquals(A, B) {
		t.Errorf("Expected tuple to not be equal with different dimensions")
	}
}

func testRandom2D4KN(n int, t *testing.T) {
	// we have a 4 square rrgions we're gonna gen points around
	// then the clusters should 100% be in those 4 regions
	// let's make a range of 10, (10x10 boxes),
	boxRange := 10.0
	startCoords := [][]float64{
		[]float64{0, 0},
		[]float64{90, 0},
		[]float64{0, 90},
		[]float64{90, 90},
	}
	numPointsInEachRange := n
	data := make([][]float64, 0, numPointsInEachRange*len(startCoords))
	for _, startTuple := range startCoords {
		for i := 0; i < numPointsInEachRange; i += 1 {
			sx, sy := rand.Float64(), rand.Float64()
			x := startTuple[0] + (sx * boxRange)
			y := startTuple[1] + (sy * boxRange)
			data = append(data, []float64{x, y})
		}
	}
	k := len(startCoords)
	result, err := FindKClusters(k, data, OptionKMeansLocKPPFlag, OptionSeedRNG(123456789), OptionSortResults)
	//result, err := FindKClusters(k, data)

	if err != nil {
		t.Fatal(err)
	}
	for _, cluster := range result.Clusters {
		found := false
		for idx, possRegion := range startCoords {
			loc := cluster.Loc
			centerOfRegion := []float64{possRegion[0] + (boxRange / 2), possRegion[1] + (boxRange / 2)}
			if tupleEqualsWithinTol(loc, centerOfRegion, boxRange/2) {
				found = true
				startCoords = append(startCoords[:idx], startCoords[idx+1:]...)
				break
			}
		}
		if !found {
			t.Errorf("Could not find a valid region for cluster located at %v", cluster.Loc)
		}
	}
}

func TestRandom2D4K1000(t *testing.T) {
	testRandom2D4KN(1000, t)
}
func TestRandom2D4K10000(t *testing.T) {
	testRandom2D4KN(10000, t)
}

func Test2D2K(t *testing.T) {
	data := [][]float64{
		[]float64{0, 0},
		[]float64{0, 1},
		[]float64{10, 0},
		[]float64{10, 1},
	}
	k := 2
	result, err := FindKClusters(k, data, OptionKMeansLocKPPFlag, OptionSeedRNG(123456789))
	if err != nil {
		t.Fatal(err)
	}
	// should be 2 clusters optimal
	if len(result.Clusters) != k {
		t.Fatalf("Was expecting clusters of k: %d, not %d", k, len(result.Clusters))
	}

	centerA := []float64{0, 0.5}
	centerB := []float64{10, 0.5}
	aIdx, bIdx := 0, 1
	clusters := result.Clusters
	if tupleEquals(centerA, clusters[1].Loc) {
		aIdx, bIdx = 1, 0
	}
	if !tupleEquals(centerA, clusters[aIdx].Loc) {
		t.Fatalf("Was expecting %v, got %v", centerA, clusters[aIdx].Loc)
	}
	if !tupleEquals(centerB, clusters[bIdx].Loc) {
		t.Fatalf("Was expecting %v, got %v", centerB, clusters[bIdx].Loc)
	}

}

func Test2DFindK(t *testing.T) {
	data := [][]float64{
		[]float64{0, 0},
		[]float64{0, 1},
		[]float64{10, 0},
		[]float64{10, 1},
	}
	k := 2
	result, err := FindClusters(data)
	if err != nil {
		t.Fatal(err)
	}
	// should be 2 clusters optimal
	if len(result.Clusters) != k {
		t.Fatalf("Was expecting clusters of k: %d, not %d", k, len(result.Clusters))
	}

	centerA := []float64{0, 0.5}
	centerB := []float64{10, 0.5}
	aIdx, bIdx := 0, 1
	clusters := result.Clusters
	if tupleEquals(centerA, clusters[1].Loc) {
		aIdx, bIdx = 1, 0
	}
	if !tupleEquals(centerA, clusters[aIdx].Loc) {
		t.Fatalf("Was expecting %v, got %v", centerA, clusters[aIdx].Loc)
	}
	if !tupleEquals(centerB, clusters[bIdx].Loc) {
		t.Fatalf("Was expecting %v, got %v", centerB, clusters[bIdx].Loc)
	}

}
